import org.apache.commons.cli.*;

import java.io.IOException;

public class ControlInterface {

    private Options options = null;

    public ControlInterface(){

        Option commandOption = Option.builder("c")
                .longOpt("command")
                .required(false)
                .numberOfArgs(1)
                .desc("")
                .build();

        Option dirFromOption = Option.builder("f")
                .longOpt("from")
                .required(false)
                .numberOfArgs(1)
                .desc("")
                .build();

        Option dirToOption = Option.builder("t")
                .longOpt("to")
                .required(false)
                .numberOfArgs(1)
                .desc("")
                .build();

        Option fileParseOption = Option.builder("p")
                .longOpt("parse")
                .required(false)
                .numberOfArgs(1)
                .desc("")
                .build();

        Option helpOption = Option.builder("h")
                .longOpt("help")
                .required(false)
                .numberOfArgs(0)
                .desc("Вызов справки")
                .build();

        options = new Options();
        options.addOption(commandOption);
        options.addOption(dirFromOption);
        options.addOption(dirToOption);
        options.addOption(fileParseOption);
        options.addOption(helpOption);
    }

    public void startApp(String[] arg){

        System.out.print("\n");
        CommandLineParser cmdLineParser = new DefaultParser();
        try {
            CommandLine cmdLine = cmdLineParser.parse(options, arg);
            String dirFrom = null;
            String dirTo = null;
            String commandLine = null;
            String fileParse = null;

            if (cmdLine.hasOption("command") && cmdLine.hasOption("from") && cmdLine.hasOption("to") && cmdLine.hasOption("parse")){
                dirFrom = cmdLine.getOptionValue("from");
                dirTo = cmdLine.getOptionValue("to");
                commandLine = cmdLine.getOptionValue("command");
                fileParse = cmdLine.getOptionValue("parse");
                ToolsFileMover tools = new ToolsFileMover(dirFrom, dirTo, fileParse, commandLine);
                Controler controler = new Controler(tools);
                controler.run();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
