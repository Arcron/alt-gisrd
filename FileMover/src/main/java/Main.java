import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        ControlInterface cli = new ControlInterface();
        cli.startApp(args);
    }
}
