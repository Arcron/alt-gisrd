import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class WriteFile implements WriteInDoc{

    private Workbook workbook = new SXSSFWorkbook(1000);
    private Sheet sheet = null;
    private int countRow = 0;
    private String file_to_excel;
    private static final Logger logger = Logger.getLogger(WriteFile.class);

    WriteFile(String file_to_excel){

        String tempFileName = getTimeFileName(file_to_excel);
        File dirFile = new File("Files");
        if (!dirFile.exists()){
            dirFile.mkdir();
        }
        File tempFile = new File(dirFile, tempFileName);

        this.file_to_excel = tempFile.getPath();
    }

    public String getTimeFileName(String fileName){

        Calendar c = Calendar.getInstance();

        int year = c.get(c.YEAR);
        String month = String.valueOf(c.get(c.MONTH)+1);
        if (month.length() == 1) month = "0" + month;
        int day = c.get(c.DAY_OF_MONTH);
        int hour = c.get(c.HOUR_OF_DAY);
        int minute = c.get(c.MINUTE);
        int sec = c.get(c.SECOND);

        String newFileName = fileName + "(" + year + "-" + month + "-" + day + "_" + hour + "-" + minute + "-" + sec +").xls";
        return newFileName;
    }

    public void writeCheckFile(ArrayList<String> arrayList, boolean newList, String sheetName) throws FileNotFoundException {

        if (newList){
            sheet = workbook.createSheet();
            int temp = workbook.getNumberOfSheets();
            workbook.setSheetName(temp-1, sheetName);
            countRow = 0;
        } else {

        }
        Row row = sheet.createRow(countRow);
        int size = arrayList.size();
        for (int i = 0; i < size; i++){
            Cell cell = row.createCell(i);
            cell.setCellValue(arrayList.get(i) == null ? "" : arrayList.get(i).toString());
        }
        countRow++;
    }

    public void close() throws IOException {

        try {
            FileOutputStream fis = new FileOutputStream(file_to_excel);
            int count = workbook.getNumberOfSheets();
            for (int i = 0; i <count; i++){
                workbook.getSheetAt(i).setDefaultColumnWidth(20);
            }
            workbook.write(fis);
            workbook.close();
        } catch (FileNotFoundException e) {
            logger.error("Файл " + file_to_excel + " не найден или не смог быть создан \n" + e);
            System.out.println("Файл " + file_to_excel + " не найден или не смог быть создан");
            throw e;
        } catch (IOException e) {
            logger.error("Не возможно записать файл " + file_to_excel + ". Возможно файл уже был закрыт \n" + e);
            System.out.println("Не возможно записать файл " + file_to_excel + ". Возможно файл уже был закрыт");
            throw e;
        }
    }
}
