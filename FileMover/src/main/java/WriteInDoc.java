import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public interface WriteInDoc {
    void writeCheckFile(ArrayList<String> arrayList, boolean newList, String sheetName) throws FileNotFoundException;
    void close() throws IOException;
}
